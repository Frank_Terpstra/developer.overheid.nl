import { StyledCard, Body, Footer, Title } from './index.styles'

const Card = StyledCard
Card.Body = Body
Card.Footer = Footer
Card.Title = Title

export default Card
